#include<stdio.h>
int main()
{
	struct student
	{
		int r_num;
		char name[30];
		float marks;
	};
	struct student stud1;
	struct student stud2;
	printf ("enter the roll number,name and marks of the student 1: \n");
	scanf ("%d%s%f",&stud1.r_num,&stud1.name,&stud1.marks);
	printf ("\n");
	printf ("enter the roll number,name and marks of the student 2 : \n");
	scanf ("%d%s%f",&stud2.r_num,&stud2.name,&stud2.marks);
	printf ("\n");
	printf ("details of student 1 : \n");
	printf ("roll number : %d\n",stud1.r_num);
	printf ("name : %s\n",stud1.name);
	printf ("marks : %f\n",stud1.marks);
	printf ("\n");
	printf ("details of student 2 : \n");
	printf ("roll number : %d\n",stud2.r_num);
	printf ("name : %s\n",stud2.name);
	printf ("marks : %f\n",stud2.marks);
	printf ("\n");
	if (stud1.marks>stud2.marks)
	{
		printf ("%s has scored the highest marks\n",stud1.name);
	}
	else
	{
		printf ("%s has scored the highest marks\n",stud2.name);
	}
	return 0;
}