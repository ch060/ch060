#include<stdio.h>
#include<conio.h>
int main()
{
	int i,n,num,pos;
	printf ("enter the number of elements in the array : ");
	scanf ("%d",&n);
	int arr[n];
	printf ("enter the values of the elements : \n");
	for (i=0;i<n;i++)
	{
		scanf ("%d",&arr[i]);
	}
	printf ("enter the number to be inserted : \n");
	scanf ("%d",&num);
	printf ("enter the position at which the number has to be entered : ");
	scanf ("%d",&pos);
	for (i=n-1;i>=pos;i--)
		arr[i+1]=arr[i];
	arr[pos]=num;
	n++;
	printf ("the array after insertion of the number %d is : \n",num);
	for(i=0;i<n;i++)
		printf ("\t%d",arr[i]);
	return 0;
}