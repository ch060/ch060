#include<stdio.h>
int main()
{
	int i,n,num,beg,end,mid,found=0;
	printf ("enter the number of elements in the array : \n");
	scanf ("%d",&n);
	int arr[n];
	printf ("enter the elements of the array : \n");
	for (i=0;i<n;i++)
	{
		scanf ("%d",&arr[i]);
	}
	printf ("enter the number that has to be searched : \n");
	scanf ("%d",&num);
	beg=0,end=n-1;
	while (beg<=end)
	{
		mid=(beg+end)/2;
		if (arr[mid]==num)
		{
			printf ("%d is present in the array at the position %d\n",num,mid);
			found=1;
			break;
		}
		else if (arr[mid]>num)
			end=mid-1;
		else
			beg=mid+1;
	}
	if (beg>end&&found==0)
		printf ("the number %d does not exist in the array\n",num);
	return 0;
}