#include<stdio.h>
int main()
{
	struct DATE
	{
		int day;
		int month;
		int year;
	};
	struct employee
	{
		int emp_id;
		char emp_name[100];
		float salary;
		struct DATE date;
	};
	struct employee employee1;
	printf ("enter the details of employee1 : ");
	printf ("\nenter the id no of the employee : ");
	scanf ("%d",&employee1.emp_id);
	printf ("enter the name of the employee : ");
	scanf ("%s",&employee1.emp_name);
	printf ("enter the salary of the employee : ");
	scanf ("%f",&employee1.salary);
	printf ("enter the date of joining of the employee ");
	scanf ("%d%d%d",&employee1.date.day,&employee1.date.month,&employee1.date.year);
	printf ("\n");
	printf ("id no = %d\n",employee1.emp_id);
	printf ("name = %s\n",employee1.emp_name);
	printf ("salary = %f\n",employee1.salary);
	printf ("date of joining = %d-%d-%d",employee1.date.day,employee1.date.month,employee1.date.year);
	return 0;
}