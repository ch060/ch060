#include<stdio.h>
#include<math.h>
int main()
{
    float a,b,c,det,d1,d2;
    printf ("enter the values of a,b and c");
    scanf ("%f%f%f",&a,&b,&c);
    det=(pow(b,2)-(4*a*c));
    if (det>0)
    {
        printf ("the roots are real and imaginary\n");
        d1=(-b+sqrt(det))/(2*a);
        d2=(-b-sqrt(det))/(2*a);
        printf ("the roots are %f and %f",d1,d2);
    }
    else if (det==0)
    {
        printf ("the roots are real and equal\n");
        d1=(-b+sqrt(det))/(2*a);
        d2=(-b-sqrt(det))/(2*a);
        printf ("the roots are %f and %f",d1,d2);
    }
    else
    {
        printf ("the roots are complex\n");
        d1=(-b+sqrt(-det))/(2*a);
        d2=(-b-sqrt(-det))/(2*a);
        printf ("the roots are %f+i%f and %f-i%f\n",-b/(2*a),sqrt(-det)/(2*a),-b/(2*a),sqrt(-det)/(2*a));
    }
    return 0;
}