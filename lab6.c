#include<stdio.h>
#include<conio.h>
int main()
{
	int i,n,temp,arr[20];
	int small,small_pos,large,large_pos;
	printf ("enter the number of elements : \n");
	scanf ("%d",&n);
	for (i=0;i<n;i++)
	{
		printf ("enter the value of element %d : ",i);
		scanf ("%d",&arr[i]);
	}
	small=arr[0];
	small_pos=0;
	large=arr[0];
	large_pos=0;
	for(i=1;i<n;i++)
	{
		if (arr[i]<small)
		{
			small=arr[i];
			small_pos=i;
		}
		if (arr[i]>large)
		{
			large=arr[i];
			large_pos=i;
		}
	}
	printf ("the smallest and the largest of the numbers are : %d and %d\n",small,large);
	printf ("the position of the smallest and the largest number in the array is : %d and %d\n",small_pos,large_pos);
	temp=arr[large_pos];
	arr[large_pos]=arr[small_pos];
	arr[small_pos]=temp;
	printf ("the new array is : \n");
	for (i=0;i<n;i++)
		printf ("%d\n",arr[i]);
	return 0;
}