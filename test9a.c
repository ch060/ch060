#include<stdio.h>
void swap(int *a,int *b)
{
    int t;
    t=*a;
    *a=*b;
    *b=t;
}
int main()
{
    int num1,num2;
    printf ("enter the value of number 1 and number 2 : ");
    scanf ("%d%d",&num1,&num2);
    printf ("before swapping the numbers are : num1 is %d and num2 is %d\n",num1,num2);
    swap (&num1,&num2);
    printf ("after swapping the numbers are : num1 is %d and num2 is %d\n",num1,num2);
    return 0;
}