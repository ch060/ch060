#include<stdio.h>
int exp(int a,int b);
int main()
{
    int a,b,res;
    printf ("enter the value of a and b : ");
    scanf ("%d%d",&a,&b);
    res=exp(a,b);
    printf ("\n%d to the power %d is %d",a,b,res);
    return 0;
}
int exp(int a,int b)
{
    register int res=1;
    int i;
    for (i=1;i<=b;i++)
        res=res*a;
    return res;
}